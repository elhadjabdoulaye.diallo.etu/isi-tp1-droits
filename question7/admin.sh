#!/bin/bash

mkdir "dir_a"
mkdir "dir_b"
mkdir "dir_c"

chown admin.groupe_a dir_a
chown admin.groupe_b dir_b

touch dir_a/test.txt
touch dir_b/test.txt
touch dir_c/test.txt

chmod +t dir_c
chmod +t dir_a
chmod +t dir_b

chmod o-x dir_b
chmod o-x dir_a