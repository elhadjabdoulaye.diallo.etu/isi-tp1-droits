#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
    FILE *f;
    struct stat status;
    int c;
    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }
    fprintf(stdout, "EUID = %u, EGID = %u, RUID = %u RGID = %u\n",
            geteuid(), getegid(), getuid(), getgid());

    stat("prog", &status);

    f = fopen(argv[1], "r");
    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    printf("File opens correctly\n");
    fprintf(stdout, "File content \n");
    while ((c=getc(f))!=EOF){
        fputc(c, stdout);
    }
    fclose(f);
    exit(EXIT_SUCCESS);
}
