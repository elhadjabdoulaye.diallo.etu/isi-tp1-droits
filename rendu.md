### Rendu "Les droits d’accès dans les systèmes UNIX"

#### Binome

- DIALLO, Elhadj Abdoulaye, email: elhadjabdoulaye.diallo.etu@univ-lille.fr

### Question 1

Le processus peut écrire parce que son EUID est égal à l'UID qui appartient au même groupe que l'utilisateur ubuntu.

### Question 2

- Pour un répertoire, le x signifie que le parcours de répertoire est autorisé.

- Quand on essaie d'y entrer, on "Permission non accordée"

- ls: impossible d'accéder à 'mydir/.': Permission non accordée
  ls: impossible d'accéder à 'mydir/data.txt': Permission non accordée
  ls: impossible d'accéder à 'mydir/..': Permission non accordée
  total 0
  d????????? ? ? ? ?              ? .
  d????????? ? ? ? ?              ? ..
  -????????? ? ? ? ?              ? data.txt

### Question 3

- EUID = 1002, EGID = 1003, RUID = 1002 RGID = 1003

Oui comme l'exécutable appartient à l'utilisateur ubuntu et au même groupe ubuntu.

- EUID = 1002, EGID = 1003, RUID = 1002 RGID = 1003
  
Oui, il arrive à ouvrir le fichier en lecture.
### Question 4

EUID = 1001 EGID =  1003 RUID =  1001 RGID =  1003

### Question 5

- chfn permet de changer le nom complet des utilisateurs.

-rwsr-xr-x 1 root root 85064 juil. 15  2021 /usr/bin/chfn

chfn appartient au superuser (root) avec set-UID activé.

Les permissions indiquent :

- Le user peut lire, écrire et exécuter la commande
- Le groupe peut lire et exécuter tout simplement
- Les autres peuvent seulement exécuter la commande
### Question 6
La plupart des systèmes linux utilisent maintenant la technique des ***shadow passwords***. 
Le mot de passe crypté n'est plus stocké dans ***/etc/passwd*** mais dans un autre fichier, comme ***/etc/shadow***
accessible seulement en lecture par un processus ayant un UID nul.
### Question 7

Mettre les scripts bash dans le repertoire *question7*.

### Question 8

Le programme et les scripts dans le repertoire *question8*.

### Question 9

Le programme et les scripts dans le repertoire *question9*.

### Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








