//
// Created by lion on 22/01/2022.
//
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifndef ISI_TP1_DROITS_CHECK_PASS_H
#define ISI_TP1_DROITS_CHECK_PASS_H
int checkGroup(gid_t user_gid, gid_t file_gid);
int checkPasswd(char *passwd, gid_t user_guid);
char *getPasswd(char *line, gid_t user_guid);
int getGuid(char *line);
void askForPasswd();
#endif //ISI_TP1_DROITS_CHECK_PASS_H
