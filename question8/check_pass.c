//
// Created by lion on 22/01/2022.
//

#include "check_pass.h"
char pass[255];

int checkGroup(gid_t user_gid, gid_t file_gid){
    return user_gid == file_gid;
}

int getGuid(char *line){
    char guid[5];
    int c = 0;
    while (line[c] != ':'){
        guid[c] = line[c];
        c ++;
    }
    return atoi(guid);
}
char *getPasswd(char *line, gid_t user_guid){
    int c = 0, cpt =0;
    while (line[c] != ':'){
        c ++;
    }
    c ++;
    if(getGuid(line) == user_guid) {
        for (int i = c; line[i] != '\0'; i++) {
            pass[cpt++] = line[i];
        }

        return pass;
    }
    return NULL;
}

int checkPasswd(char *passwd, gid_t user_gid){
    FILE *file;
    char passwd_info[255];
    char *pwd;
    file = fopen("/home/admin/passwd", "r+");
    if (file == NULL) {
        perror("/home/admin/passwd");
        exit(EXIT_FAILURE);
    }
    while (fgets(passwd_info, 255, file) != NULL){
        pwd = getPasswd(passwd_info, user_gid);
        if(pwd != NULL) {
            return strcmp(passwd, pwd);
        }
    }
    fprintf(file, "%u%c%s", user_gid, ':', passwd);
    fclose(file);

    return -1;
}

