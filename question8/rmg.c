//
// Created by lion on 22/01/2022.
//

#include <stdio.h>
#include <stdlib.h>
#include "check_pass.h"

int main(int argc, char *argv[]){
    struct stat status;
    char user_passwd[255];
    stat("rmg", &status);
    gid_t guid = getgid();
    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }


    if(checkGroup(status.st_gid, guid)){
        printf("Saisir un mot de passe : \n");
        while (fgets(user_passwd, 255, stdin) == NULL){
            printf("Saisir un mot de passe : \n");
        }
        if(!checkPasswd(user_passwd, getgid())){
            if(access(argv[1], F_OK)){
                if(unlink(argv[1]))
                    fprintf(stdout, "Le fichier %s a été supprimé\n", argv[1]);
                else{
                    fprintf(stdout, "Impossible de supprimer le fichier %s\n", argv[1]);
                }
            } else{
                fprintf(stdout, "Le fichier %s n'est pas accessible\n", argv[1]);
            }
        }
    } else{
        fprintf(stderr, "Permission refusée, vous n'avez pas le droit de lancer ce programme !\n");
        exit(EXIT_FAILURE);
    }

    return 0;
}
